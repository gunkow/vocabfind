# Vocab find

### requirements:
- python3 available via `python3`
### how to run:
simply
#### `sh run.sh` <br>
- creates venv;<br> 
- runs `pytest test.py`
- runs generator `run_generator.py` and highlighter `run_highlighter.py` services(on ports `4000`, `4001`) in background
- runs client, which sends requests to services via *json-rpc*<br>
  with console input<br>
- and purges venv and services after


`highlighter.py` - file with main logic

estimations:
 - word area size  <= 100x100
 - dictionary with over 100k words
 <br><br>
**highlighting algorithm:**
- uses transpose and diagonal shift matrix transformations to be able to use simple per row search
- per row:
  - splits row to all possible substrings of all lengths (on row size 100 - around 5k substrings)
  - searches each substring in vocabulary(sortedList)
  - highlights each found word
- reverse matrix transform
 
for 100x100 puzzle it takes arount 10sec on my computer.<br> I am able to speed it up more by using less transformations between strings and numpy array. but that is it for now.


Have a nice day!

