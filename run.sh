echo "%%%% create venv %%%%%%"
python3 -m venv vvenv
venv/bin/pip install -r requirements.txt
venv/bin/pytest test.py
nohup vvenv/bin/python run_generator.py &
nohup vvenv/bin/python run_highlighter.py &

venv/bin/python run_client.py

echo "%%%% kill pythons %%%%%%"
pkill -f run_generator.py
pkill -f run_highlighter.py
echo "%%%%% remove venv %%%%%%%"
rm -r vvenv