import requests
import json

generator_url = "http://localhost:4000/jsonrpc"
highlighter_url = "http://localhost:4001/jsonrpc"


def main():
    def perform(length):
        payload = {
            "method": "generate",
            "params": [{"length": length}],
            "jsonrpc": "2.0",
            "id": 0,
        }
        response = requests.put(generator_url, json=payload).json()

        matrix = response['result']
        payload = {
            "method": "highlight",
            "params": [{"matrix": matrix}],
            "jsonrpc": "2.0",
            "id": 0,
        }
        response = requests.put(highlighter_url, json=payload).json()
        print("HIGHLIGHTED matrix:")
        print(response['result']['matrix'])
        print("FOUND WORDS:")
        print(response['result']['words'])

    stop = False
    while not stop:
        try:
            print()
            print("input the number of desired square size \nOR non int to exit:")
            length = int(input())
            perform(length)
        except:
            stop = True


if __name__ == "__main__":
    main()
