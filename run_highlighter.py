from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple
from jsonrpc import JSONRPCResponseManager, dispatcher
import highlighter
import numpy


@Request.application
def application(request):
    def handle(a):
        matrix = a["matrix"]
        outmatrix, words = highlighter.highlight(numpy.array(matrix))
        outmatrix_string = highlighter.display(outmatrix)
        return dict(matrix=outmatrix_string, words=list(words))

    dispatcher["highlight"] = handle

    response = JSONRPCResponseManager.handle(
        request.data, dispatcher)
    return Response(response.json, mimetype='application/json')


if __name__ == '__main__':
    run_simple('0.0.0.0', 4001, application)
