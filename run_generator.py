from typing import Dict
from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple
from jsonrpc import JSONRPCResponseManager, dispatcher
import generator
import numpy

matrixes: Dict[int, numpy.ndarray] = {}


@Request.application
def application(request):
    def handle(a):
        length = a["length"]
        matrix = generator.generate(length)
        matrixes[length] = matrix
        return matrix.tolist()

    dispatcher["generate"] = handle

    response = JSONRPCResponseManager.handle(
        request.data, dispatcher)
    return Response(response.json, mimetype='application/json')


if __name__ == '__main__':
    run_simple('0.0.0.0', 4000, application)
