import sortedcontainers
import highlighter
import numpy as np

VOCAB = sortedcontainers.SortedList(["abba", "bat"])


def test_search():
    result = highlighter.smart_search("abbatteryabba", VOCAB)
    assert dict(result) == {'abba': [0, 9], 'bat': [2]}


def test_highlight():
    matrix = np.vectorize(lambda i: ord(i))(
        np.array([['m', 'n', 'k'],
                  ['t', 'o', 't'],
                  ['r', 'b', 'm']]))
    result, words = highlighter.highlight(matrix)
    expected = np.vectorize(lambda i: ord(i))(
        np.array([['m', '.', '.'],
                  ['t', 'o', 't'],
                  ['.', '.', 'm']]))

    assert words == {'mom', 'tot'}
    assert (expected == result).all()
