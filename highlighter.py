from collections import defaultdict
from typing import List, Callable, Tuple, Dict, Set
import numpy as np
from sortedcontainers import SortedList

with open("words.txt") as file:
    VOCABULARY = [i.strip() for i in file.readlines() if len(i) > 3]
    VOCABULARY = SortedList(VOCABULARY)


def find_sortedlist(sortedlist, key, start=None):
    try:
        return sortedlist.index(key, start)
    except ValueError:
        return -1


def smart_search(rowstring: str, vocab: SortedList) -> Dict[str, List[int]]:
    """
    :param vocab:
    :param matrix row as string:
    :return: dict of <word -> list of indexes of word start>
    """
    word_index_dict: Dict[str, List[int]] = defaultdict(list)

    def substrings(string, length) -> List[Tuple[int, str]]:
        return [(i, string[i:i + length]) for i in range(len(string) - length + 1)]

    all_substrings: List[Tuple[int, str]] = [substring for i in range(3, len(rowstring) + 1)
                                             for substring in substrings(rowstring, i)]
    for i, substring in all_substrings:
        if find_sortedlist(vocab, substring) != -1:
            word_index_dict[substring].append(i)

    return word_index_dict


def diagonal_shift(matrix, reverse=False):
    """
    :param matrix:
    :param reverse transform
    :return: matrix with diagonal shift - size(2n-1, n)
    """
    n = matrix.shape[1]

    to_diagonal: Callable[[int, int], Tuple[int, int]] = lambda yx: ((yx[0] - yx[1] + n - 1) % n, yx[1])
    from_diagonal: Callable[[int, int], Tuple[int, int]] = lambda yx: ((yx[0] + yx[1] - (n - 1)) % n, yx[1])

    diag_matrix = np.zeros((n + (n - 1), n), int)
    mapping = to_diagonal if not reverse else from_diagonal
    iter = np.nditer(matrix, flags=['multi_index'])
    while not iter.finished:
        diag_matrix[mapping(iter.multi_index)] = iter[0]
        iter.iternext()
    return diag_matrix


def refine(matrix) -> Tuple[np.ndarray, List[str]]:
    """
    :param matrix:
    :return: refined matrix of the horizontally detected words
    """

    def refine_row(rowstring: str, word_index: Dict[str, List[int]]) -> Tuple[str, List[str]]:
        refined_row = "." * len(rowstring)
        for word, index_list in word_index.items():
            for index in index_list:
                refined_row = "".join((refined_row[:index], word, refined_row[index + len(word):]))
        return refined_row, list(word_index.keys())

    n = matrix.shape[1]
    refined_matrix = np.empty((0, n), int)
    allwords = []
    for row in matrix:
        row_string = ''.join(chr(i) for i in row)
        word_index_dict = smart_search(row_string, VOCABULARY)
        refined_row_string, words = refine_row(row_string, word_index_dict)
        allwords.extend(words)
        refined_matrix = np.vstack([refined_matrix, [ord(ch) for ch in refined_row_string]])
    return refined_matrix, allwords


def highlight(matrix: np.ndarray) -> Tuple[np.ndarray, Set[str]]:
    """
    :param matrix of chars as ints
    :return: refined matrix of only present words
    """
    n = matrix.shape[1]

    allwords: Set[str] = set()

    horizontal_match_matrix, words = refine(matrix)
    allwords.update(words)

    tmp, words = refine(matrix.T)
    vertical_match_matrix = tmp.T
    allwords.update(words)

    tmp, words = refine(diagonal_shift(matrix))
    diagonal_match_matrix_1 = diagonal_shift(tmp, reverse=True)[:n]
    allwords.update(words)

    tmp, words = refine(diagonal_shift(matrix, reverse=True))
    diagonal_match_matrix_2 = diagonal_shift(tmp)[:n]
    allwords.update(words)

    overlay = np.where(horizontal_match_matrix != 46, horizontal_match_matrix, vertical_match_matrix)
    overlay = np.where(overlay != 46, overlay, diagonal_match_matrix_1)
    overlay = np.where(overlay != 46, overlay, diagonal_match_matrix_2)
    return overlay, allwords


def display(charmatrix) -> str:
    out = ""
    for row in charmatrix:
        out += " ".join(chr(i) for i in row) + '\n'
    return out
