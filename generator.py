from typing import Dict
import numpy as np

matrixes: Dict[int, np.ndarray] = {}


def generate(length: int, seed=None):
    if seed:
        np.random.seed(seed)
    matrix = np.random.randint(ord('a'), ord('z'), (length, length))
    matrixes[length] = matrix
    return matrix


def display(charmatrix) -> str:
    out = ""
    for row in charmatrix:
        out += " ".join(chr(i) for i in row) + '\n'
    return out